import json
import numpy as np
import os

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as fnc


class ANNNetwork(nn.Module):
    def __init__(
        self,
        input_shape,  # input channels, dim_x, dim_y
        output_shape,  # output neurons
        device,
        lr=10e-3,  # learning rate
        name="prediction.pt",  # network's name checkpoint
        chkpt_dir="./tmp"):  # path network's checkpoint

        super(ANNNetwork, self).__init__()
        self.device = device
        self.chkpt_dir = chkpt_dir
        self.chkpt_name = os.path.join(self.chkpt_dir, name)
        self.conv1 = nn.Conv2d(input_shape[0], 16, 8, stride=4)
        self.conv2 = nn.Conv2d(16, 32, 2, stride=2)

        dims = self._calculate_output_conv_dim(input_shape)
        self.fc1 = nn.Linear(dims, 512)
        self.fc2 = nn.Linear(512, output_shape)

        self.optimizer = optim.RMSprop(self.parameters(), lr=lr)
        self.loss = nn.MSELoss()
        self.to(self.device)

    def _calculate_output_conv_dim(self, input_shape):
        x = torch.zeros((1, *input_shape))
        x = self.conv1(x)
        x = self.conv2(x)
        return int(np.product(x.size()))

    def forward(self, x):  # must
        x = fnc.relu(self.conv1(x))
        x = fnc.relu(self.conv2(x))  # shape: batch_size, n_filters, h, w
        x = torch.flatten(x, 1)
        x = fnc.relu(self.fc1(x))
        x = self.fc2(x)
        return x

    def save_chkpt(self, name):  # must
        name = self.chkpt_name \
            if name is None else os.path.join(self.chkpt_dir, name)
        torch.save({"model_state_dict": self.state_dict(),
                    "optimizer_state_dict": self.optimizer.state_dict()},
                    name) 

    def load_chkpt(self, load_optimizer, name):  # must
        name = self.chkpt_name \
            if name is None else os.path.join(self.chkpt_dir, name)
        checkpoint = torch.load(name, map_location="cuda")
        self.load_state_dict(checkpoint["model_state_dict"])
        if load_optimizer:
            self.optimizer.load_state_dict(checkpoint["optimizer_state_dict"])