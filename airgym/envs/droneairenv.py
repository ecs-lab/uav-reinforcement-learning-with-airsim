import numpy as np
import airsim
import time
import gym
import csv

from .alberto import movment
from .alberto import movment_old
from .dvs import EventGenerator
from .airenv import AirsimEnv


class DroneEnv(AirsimEnv):
    def __init__(
        self,
        img_shape,
        step_v,
        step_t,
        gamma_p,
        gamma_a,
        gamma_c,
        n_actions_stop,
        goal_y,
        use_join,
        device,
        get_time,
        acc,
        path_record,
        ip="",
        frames=3,
        plot=False,
        frames_to_clean=0,
        dvs_on=True):

        super(DroneEnv, self).__init__(img_shape)
        self.img_shape = img_shape
        self.step_v = step_v
        self.step_t = step_t  # in secondi
        self.gamma_a = gamma_a
        self.gamma_p = gamma_p
        self.gamma_c = gamma_c
        self.goal_y = goal_y  # verificare le unità di misura
        self.n_actions_stop = n_actions_stop
        self.frames = frames
        self.ip = ip
        self.acc = acc
        self.path_record = path_record
        self.dvs_on = dvs_on

        self.get_time = get_time

        self.frames_to_clean = frames_to_clean  # lo uso per pulire i frames

        self.action_space = gym.spaces.Discrete(5)
        self.eq_action = {
            0: (    self.step_v,    self.step_v,        0),  # sx
            1: (    -self.step_v,   self.step_v,        0),  # dx
            2: (    0,              self.step_v,        self.step_v),  # scende
            3: (    0,              self.step_v,        -self.step_v),  # sale
            4: (    0,              self.step_v,        0),  # avanti
        }

        self.dvs = EventGenerator(img_shape, plot, device=device)

        self.image_request = airsim.ImageRequest(
            "0",
            airsim.ImageType.Scene,
            False,
            False
        )

        self.drone = None
        self._start_time = None
        self._position = None
        self._position_init = None
        self._image = None
        self._collisions = 0
        self._cntr_actions = self.n_actions_stop
        self._stacked_images = None
        self._action_join = use_join
        # self._initialization()

        self._stacked_rgb = None

        self._cntr_action_acc = 1
        self.list_of_actions = ['4', '4', '2', '1', '0', '3', '0', '4', '1', '4', '1', '4', '3', '1', '0', '2', '1', '3', '1']
    
    def reconnect(self):
        self.drone = airsim.MultirotorClient(ip=self.ip)

    def _do_action(self, action):
        return movment_old(
            self.drone,
            self.eq_action[action],
            t=self.step_t,
            acc=self.acc,
            mov_com=self._action_join,
            path_record=self.path_record,
            pic=self.frames,
            img_request=self.image_request,
            rgb_image_shape=self.dvs.rgb_image_shape
        )
        
    def _initialization(self):
        # TODO mettere la possibilità di prendere le immagini anche al takeoff
        if self.dvs_on:
            self.dvs.reset()
        self.drone.reset()
        self.drone.enableApiControl(True)
        self.drone.armDisarm(True)
        # imgs, , list of sim time: time, coll: bool, _pos: sim pos
        _, _time, _, _pos = movment(  
            self.drone,
            [0, 0, -2],
            t=1,
            acc=self.acc,
            mov_com=self._action_join,
            path_record=self.path_record,
            pic=self.frames,
            img_request=self.image_request,
            rgb_image_shape=self.dvs.rgb_image_shape
        )

        """
        data:
         - join: lista di 3 elementi ciascuno dei quali è un img data da getImages
         - active: lista di 3 elementi - vedere alberto/read_img
        coll: flag delle collisioni (bool)
        """

        self._collisions = False  # TODO da verificare se airsim lo mette a False/True
        self._cntr_actions = self.n_actions_stop
        self._start_time = _time[-1]
        self._position_init = _pos
        
    def reset(self):
        # TODO mettere la possibilità di prendere le immagini anche al takeoff
        self._initialization()
        return np.zeros(([self.frames, *self.img_shape[::-1]]))

    def __del__(self):
        self.drone.reset()
    
    def _get_obs(self, img, t_img):
        if self.get_time: start_time = time.time()
        self._image = self.dvs.conversion(img, t_img - self._start_time)  # immagine dvs 
        if self.get_time: t = time.time() - start_time

        if self.get_time: return t

    def _sim_pause(self):
        if self.drone.simIsPause():
            self.drone.simPause(False)

    def _comupte_reward(self, action):
        a = 0 if action == 4 else 1
        dy = self._position.y_val - self._position_init.y_val
        r = self.gamma_p * dy + \
                self.gamma_c * self._collisions + \
                self.gamma_a * a
        done = True if self._collisions >= 1 or dy >= self.goal_y else False
        return r, done, \
            {"y": dy, "c": self._collisions, "n_actions": self._cntr_actions}

    def step(self, action):
        # action = int(self.list_of_actions[self._cntr_action_acc-1])
        folder = "garb3"
        if self.get_time:
            times = {
                "do_action_time": None,
                "simContinueForTime_time": list(),
                "get_obs_time": list(),
                "real_dvs_time": list(),
                "no_dvs": list()
            }
        self._cntr_actions -= 1

        if self.get_time:
            start_time = time.time()
        _stacked_rgb, _time, self._collisions, self._position = self._do_action(action)
        # _stacked_rgb, _time, self._collisions, self._position = self._do_action(int(self.list_of_actions[self._cntr_action_acc-1]))


        # np.save(f"./data_alberto/old_active/_stacked_rgb_{}_{self._cntr_actions}", _stacked_rgb)

        if self.get_time:
            times["do_action_time"] = time.time() - start_time

        if self.dvs_on:
            self._stacked_images = np.zeros(
                ([self.frames, *self.img_shape[::-1]]),
                dtype=np.int8
            )  # TODO non mi piace, vedere se c'è un metodo più furbo

            for i, (img, t) in enumerate(zip(_stacked_rgb, _time)):
                if self.get_time:
                    start_time = time.time()
                real_dvs_time = self._get_obs(img, t)
                if self.get_time:
                    times["get_obs_time"].append(time.time() - start_time)
                    times["real_dvs_time"].append(real_dvs_time)
                if not(self._image is None):
                    self._stacked_images[i, ...] = self._image

            np.save(f"./data_alberto/pics/{folder}/dvs_{self._cntr_action_acc}.npy", self._stacked_images)

            # _stacked_images ha le 3 dvs, salvale qua...
            
        else:
            if self.get_time:
                start_time = time.time()
            self._stacked_images = np.array(
                _stacked_rgb,
                dtype=np.float32
            ) / 255
            np.save(f"./data_alberto/pics/{folder}/gray_norm_{self._cntr_action_acc}.npy", self._stacked_images)
            if self.get_time:
                    times["no_dvs"].append(time.time() - start_time)
        
        # np.save(f"./data_alberto/pics/{folder}/pos_action.npy", [self._position.x_val, self._position.y_val, self._position.z_val, action])
        
        f = open(f"./data_alberto/pics/{folder}/pos_action.csv", 'a')
        writer = csv.writer(f)
        writer.writerow([self._position.x_val, self._position.y_val, self._position.z_val, action])

        self._cntr_action_acc += 1

        if self._cntr_actions == 0:
            self._collisions = True

        reward, done, info = self._comupte_reward(action)
        
        if self.get_time:
            info["time"] = times
        return self._stacked_images, reward, done, info

    def render(self):
        # TODO da implementare meglio - non funziona - 
        print("Warining: Use plot=True, not this method...")
        self.dvs.plot = True  # ummm
        for i in range(self.frames):
            self.dvs.plot_camera(self._stacked_images[i, ...])
