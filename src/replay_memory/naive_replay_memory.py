import json
import numpy as np
import os

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as fnc


class NaiveReplayMemory():
    def __init__(
        self,
        state_shape,
        buffer_size,
        batch_size,
        create_dataset_flag=False,
        path_to_save_data=None):
        
        self.buffer_size = buffer_size
        self.state_shape = (state_shape[0], state_shape[2], state_shape[1])
        self.batch_size = batch_size

        self.memory_states = np.zeros(
            (self.buffer_size, *self.state_shape),
            dtype=np.float32
        )  # TODO vedere come cambiare senza fare arabbiare pytorch
        self.memory_states_ = np.zeros(
            (self.buffer_size, *self.state_shape),
            dtype=np.float32
        )  # TODO vedere come cambiare senza fare arabbiare pytorch
        self.memory_actions = np.zeros(self.buffer_size,
            dtype=np.int64
        )  # TODO vedere come cambiare senza fare arabbiare pytorch
        self.memory_rewards = np.zeros(self.buffer_size,
            dtype=np.float32
        )
        self.memory_dones = np.zeros(self.buffer_size,
            dtype=np.bool
        )

        self.index_memory = 0  # serve per tenere conto della memoria occupata
        self.count_batch_memories = 0
        self.path_to_save_data = path_to_save_data
        self.create_dataset_flag = create_dataset_flag
        self.flag_save = False  # serve nel nome del numpy array

    def update_memories(self, state, action, reward, state_, done):
        index = self.index_memory % self.buffer_size
        self.memory_states[index] = state
        self.memory_states_[index] = state_
        self.memory_actions[index] = action
        self.memory_rewards[index] = reward
        self.memory_dones[index] = done
        self.index_memory += 1

        if ((self.index_memory + 1) % self.buffer_size == 0) and \
                    self.create_dataset_flag:
            self.save_data(ifBatchName=True)
            self.count_batch_memories += 1

    def save_data(self, name="", ifBatchName=False):
        name =  f"_{name}-{self.count_batch_memories}" \
            if ifBatchName else f"_{name}"
        np.save(
            os.path.join(self.path_to_save_data, f"states{name}.npy"),
            self.memory_states
        )
        np.save(
            os.path.join(self.path_to_save_data, f"states_{name}.npy"),
            self.memory_states_
        )
        np.save(
            os.path.join(self.path_to_save_data, f"actions{name}.npy"),
            self.memory_actions
        )
        np.save(
            os.path.join(self.path_to_save_data, f"rewards{name}.npy"),
            self.memory_rewards
        )
        np.save(
            os.path.join(self.path_to_save_data, f"dones{name}.npy"),
            self.memory_dones
        )

    def restore_data(self, name="", ifBatchName=False):
        name =  f"_{name}-{self.count_batch_memories}" \
            if ifBatchName else f"_{name}"
        self.memory_states = np.load(
            os.path.join(self.path_to_save_data, f"states{name}.npy")
        )
        self.memory_states_ = np.load(
            os.path.join(self.path_to_save_data, f"states_{name}.npy")
        )
        self.memory_actions = np.load(
            os.path.join(self.path_to_save_data, f"actions{name}.npy")
        )
        self.memory_rewards = np.load(
            os.path.join(self.path_to_save_data, f"rewards{name}.npy")
        )
        self.memory_dones = np.load(
            os.path.join(self.path_to_save_data, f"dones{name}.npy")
        )
        
    def get_random_memories(self):
        max_buffer = min(self.buffer_size, self.index_memory)
        indices = np.random.choice(max_buffer, self.batch_size, replace=False)
        return self.memory_states[indices], \
                self.memory_actions[indices], \
                self.memory_rewards[indices], \
                self.memory_states_[indices],\
                self.memory_dones[indices]

    def save_chkpt(self, name=""):
        self.flag_save = not self.flag_save
        self.save_data(name=name + f"{int(self.flag_save)}")
        tmp_dict = {
            "index_memory": self.index_memory,
            "count_batch_memories": self.count_batch_memories,
            "flag_save": self.flag_save
        }
        tmp_path = os.path.join(self.path_to_save_data, name + "_params.json")
        with open(tmp_path, "w") as fp:
            json.dump(tmp_dict, fp)

    def load_chkpt(self, name=""):
        tmp_path = os.path.join(self.path_to_save_data, name + "_params.json") 
        with open(tmp_path, "r") as f:
            tmp_dict = json.load(f)
        self.index_memory, self.count_batch_memories, self.flag_save = \
            tmp_dict.values()
        self.restore_data(name=name + f"{int(self.flag_save)}")     