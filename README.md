# UAV Reinforcement Learning with AirSim

In this project, you will find:
- The code to speed up the AirSim simulation
- The code to train a Convolutional Neural Network
- The code to use a Dynamic Vision Sensor with AirSim

The repository is a working-in-progress project that will be gradually filled.
