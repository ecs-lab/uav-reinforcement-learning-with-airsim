import numpy as np
import airsim
import gym

class AirsimEnv(gym.Env):
    metadata = {"render.modes": ["console"]}

    def __init__(self, image_shape):
        self.observation_space = gym.spaces.Box(
            0, 255,
            shape=image_shape,
            dtype=np.uint8
        )

    def __del__(self):
        raise NotImplementedError()

    def _get_obs(self):
        raise NotImplementedError()

    def _comupte_reward(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()

    def step(self, action):
        raise NotImplementedError()

    def render(self):
        return self._get_obs()



