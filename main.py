from src import AgentD2QN, ANNNetwork, SNNNetwork, NaiveReplayMemory
import airgym
import gym
import json
import numpy as np
import os
import torch
import argparse
import datetime
import time
from src import settings_file_creation
from src import LaunchEnv
import msgpackrpc


# TODO to clean up the code
parser = argparse.ArgumentParser()
parser.add_argument(
    "-i",
    "--ip",
    help="ip of the simulation"
)
parser.add_argument(
    "-s",
    "--cuda_sim",
    help="cuda device for the simulation"
)
parser.add_argument(
    "-n",
    "--cuda_net",
    help="cuda device for the network"
)
parser.add_argument(
    "-t",
    "--algo_type",
    help="type of algorithm"
)
parser.add_argument(
    "-a",
    "--algo_name",
    help="name of the algorithm"
)
parser.add_argument(
    "-v",
    "--acc",
    help="simulation speed up",
    type=int
)
parser.add_argument(
    "-j",
    "--use_join",
    help="data gathering with join or active methods"
)
parser.add_argument(
    "-d",
    "--dvs_on",
    help="use dvs or normal rgb images"
)
parser.add_argument(
    "-p",
    "--path",
    help="sub_main_save_folder",
    default=""
)


def creation_dir(path):
    if not os.path.isdir(path):
        os.mkdir(path)


def creation_dir_tree(settings, path_sub_main):
    settings["paths"]["sub_main_save_folder"] = path_sub_main

    if settings["paths"]["sub_main_save_folder"] == "":
        settings["paths"]["sub_main_save_folder"] = \
            settings["simulation_settings"]["algo_type"] + \
                datetime.datetime.now().strftime("%Y_%m_%d__%H_%M__%S_%f")
    path = os.path.join(
        os.getcwd(),
        settings["paths"]["main_save_folder"],
        settings["paths"]["sub_main_save_folder"]
    )
    
    settings["paths"]["path"] = path
    settings["paths"]["path_networks"] = os.path.join(path, "networks")
    settings["paths"]["path_graphs"] = os.path.join(path, "graphs")
    settings["paths"]["path_dataset"] = os.path.join(path, "dataset")
    if settings["droneairenv_settings"]["use_join"]:
        settings["airsim_settings"]["airsim_settings_file"] \
            ["Recording"]["Folder"] = os.path.join(path, "data_gathering")

    creation_dir(path)
    creation_dir(settings["paths"]["path_networks"])
    creation_dir(settings["paths"]["path_dataset"])
    creation_dir(settings["paths"]["path_graphs"])

    if settings["droneairenv_settings"]["use_join"]:
        creation_dir(
            settings["airsim_settings"]["airsim_settings_file"]\
                ["Recording"]["Folder"]
        )
    return settings


# TODO low cohesion: write better
def experiment(settings_path):
    # Settings and paths
    with open(settings_path, "r") as fp:
        settings = json.load(fp)

    tmp_path = os.path.join(settings["paths"]["path"], "general_settings.json")
    with open(tmp_path, "w") as fp:
        json.dump(settings, fp)

    # --- Usefull variables
    eval_mode = settings["simulation_settings"]["eval_mode"]
    save_chkpt = settings["checkpoint_settings"]["save_chkpt"]
    ngames_save = settings["utils"]["ngames_save"]
    ngames_save_chkpt = settings["checkpoint_settings"]["ngames_save_chkpt"]
    get_time = settings["utils"]["get_time_stats"]
    acc = settings["airsim_settings"]["airsim_settings_file"]["ClockSpeed"]
    if settings["droneairenv_settings"]["use_join"]:
        path_record = settings["airsim_settings"]["airsim_settings_file"] \
            ["Recording"]["Folder"] 
    else:
        path_record = None

    # --- Environment (drone) --- 
    env = gym.make(
        "airsim-drone-sample-v0",
        device=f"cuda:{settings['simulation_settings']['cuda']}",
        get_time=get_time,
        acc=acc,
        path_record=path_record,
        **settings["droneairenv_settings"],
    )

    # --- Setup some params for simulation ---
    device = torch.device(
        f"cuda:{settings['simulation_settings']['cuda']}" \
            if torch.cuda.is_available() \
                else "cpu"
    )

    n_inputs = (
        settings["droneairenv_settings"]["frames"],
        *env.observation_space.shape
    )
    n_actions = env.action_space.n

    if settings["simulation_settings"]["algo_type"] == "ann":
        net_model = ANNNetwork
    elif settings["simulation_settings"]["algo_type"] == "snn":
        net_model = SNNNetwork
    else:
        raise NotImplementedError
    
    if eval_mode:
        eps, eps_end, eps_dec = 0, 0, 0
        nGames = settings["simulation_settings"]["nGames_eval"]
        replace = None
    else:
        eps, eps_end, eps_dec = settings["simulation_settings"]["eps"]
        nGames = settings["simulation_settings"]["nGames_train"]
        replace = settings["simulation_settings"]["replace"]

    idxStartGame = settings["simulation_settings"]["idxStartGame"]
    
    # --- Q-networks ---
    name_tmp = \
        settings["simulation_settings"]["env_name"] + "_" + \
            settings["simulation_settings"]["algo_name"]

    q_next = net_model(
        n_inputs,
        n_actions,
        lr=settings["simulation_settings"]["lr"],
        device=device,
        name=name_tmp + "_q_next.pt",
        chkpt_dir=settings["paths"]["path_networks"]
    )

    q_eval = net_model(
        n_inputs,
        n_actions,
        lr=settings["simulation_settings"]["lr"],
        device=device,
        name=name_tmp + "_q_eval.pt",
        chkpt_dir=settings["paths"]["path_networks"]
    )

    # --- Replay Memory ---
    memory = NaiveReplayMemory(
        n_inputs,
        buffer_size=settings["simulation_settings"]["buffer_size"],
        batch_size=settings["simulation_settings"]["batch_size"],
        create_dataset_flag=settings["utils"]["create_dataset_flag"],
        path_to_save_data=settings["paths"]["path_dataset"]
    )

    # --- Agent ---
    agent = AgentD2QN(
        n_actions=env.action_space.n,
        replace=replace,
        batch_size=settings["simulation_settings"]["batch_size"],
        q_next=q_next,
        q_eval=q_eval,
        memory=memory,
        gamma=settings["simulation_settings"]["gamma"],
        eps=eps,
        eps_end=eps_end,
        eps_dec=eps_dec,
        chkpt_dir=settings["paths"]["path_networks"]
    )

    # Setup the enviroment class
    run_env_flag = True
    run_env = LaunchEnv(
        path_env=settings["airsim_settings"]["airsim_env_path"],
        path_exp=settings["paths"]["path"],
        launch_settings=settings["airsim_settings"]["airsim_env_args"],
        name_file_settings="env_settings.json",
        reset_env=settings["utils"]["reset_env"]
    )

    # --- Some stats --- 
    best_moves = np.inf
    best_collisions = np.inf
    best_score = -np.inf
    scores, scores_avg, eps_history = list(), list(), list()
    info_dy, info_c = list(), list()
    moves = list()
    cntr_wins = 0
    if get_time:
        times = list()

    # --- Data load in case of load chkp or eval mode --- 
    if eval_mode:
        agent.load_models()
    elif settings["checkpoint_settings"]["load_chkpt"]:
        agent.load_models(
            load_optimizer=True,
            name=settings["checkpoint_settings"]["save_name_chkpt"][0]
        )
        tmp_path = os.path.join(
            settings["paths"]["path_networks"],
            settings["checkpoint_settings"]["save_name_chkpt"][1] + ".json"
        )
        with open(tmp_path, "r") as fp:  # TODO creare una funzione
            data_to_load = json.load(fp)
            idxStartGame = data_to_load["idxStartGame"]
        
        dict_tmp = np.load(
            os.path.join(settings["paths"]["path_graphs"], "data.npy"),
            allow_pickle=True
        )[()]
        best_moves, best_collisions, best_score, scores, moves,\
            info_dy, info_c, scores_avg, eps_history = dict_tmp.values()
    
     # --- Start the games --- 
    for game in range(idxStartGame, nGames):            
        print(
            f"[start game] game {game}\n  [running the environment]  ",
            end="",
            flush=True
        )

        if run_env(run_env_flag):
            env.reconnect()

        run_env_flag = False
        done = False
        score = 0
        step_cntr = 0


        # new 
        # try:
        #     os.kill(run_env.env_subprocess.pid, 0)
        # except OSError:
        #     run_env_flag = True
        # else:
        #     state = env.reset()
        # old
        try:
            state = env.reset()
        except msgpackrpc.error.TimeoutError:
            run_env_flag = True

        if get_time:
            time_tmp = {
                "steps": list(),
                "learns": list(),
                "choose_action": list()
            }
        
        if not run_env_flag:
            print("  [playing] ", end="", flush=True)

        # --- One game ---
        while not done and not run_env_flag:
            print(".", end="", flush=True)
            if get_time:
                start_time = time.time()
            action = agent.choose_action(state)
            if get_time:
                time_tmp["choose_action"].append(time.time() - start_time)

            
            # new 
            # try:
            #     os.kill(run_env.env_subprocess.pid, 0)
            # except OSError:
            #     run_env_flag = True
            # else:
            #     state_, reward, done, info = env.step(action)
            #     if get_time:
            #         time_tmp["steps"].append(info["time"])
            # old
            try:
                state_, reward, done, info = env.step(action)
                if get_time:
                    time_tmp["steps"].append(info["time"])
            except msgpackrpc.error.TimeoutError:
                run_env_flag = True

            if not run_env_flag:
                if not eval_mode:
                    agent.store_memories(
                        state,
                        action,
                        reward,
                        state_,
                        int(done)
                    )
                    if get_time:
                        start_time = time.time()
                    agent.learn()
                    if get_time:
                        time_tmp["learns"].append(time.time() - start_time)

                state = state_
                score += reward
                step_cntr += 1
        times.append(time_tmp)
        print("")

        # --- Stats --- 
        scores.append(score)
        moves.append(step_cntr)
        tmp_avg = 0 if step_cntr == 0 else score/step_cntr
        scores_avg.append(tmp_avg)
        info_dy.append(info["y"])
        info_c.append(info["c"])
        if info["y"] >= settings["droneairenv_settings"]["goal_y"]:
            cntr_wins += 1
        avg_score = np.mean(scores_avg[-ngames_save:])

        print(
            "  [info] step cntr: ", step_cntr,
            " score: %.1f" % scores_avg[-1], 
            " average score %.1f" % avg_score,
            " best average score %.2f" % best_score,
            " epsilon %.2f" % agent.eps,
            " wins: ", cntr_wins, "/", game + 1,
            " distance: %.2f" % info["y"]
        )

        # --- Save if best model --- 
        if avg_score > best_score:
            if not eval_mode:
                print("  [saving] best model")
                agent.save_models()
            best_score = avg_score

        # --- Save data stats --- 
        eps_history.append(agent.eps)
        if game % ngames_save == 0 or eval_mode:
            print("  [saving] data")

            dict_to_save = {
                "best_moves": best_moves,
                "best_collisions": best_collisions,
                "best_score": best_score,
                "scores": scores,
                "moves": moves,
                "info_dy": info_dy,
                "info_c": info_c,
                "score_avg": scores_avg,
                "eps_history": eps_history
            }
            if get_time:
                dict_to_save["times"] = times
            np.save(
                os.path.join(settings["paths"]["path_graphs"], "data.npy"),
                np.array(dict_to_save)
            )

        # --- Save chkpt --- 
        if game % ngames_save_chkpt == 0 and not eval_mode and save_chkpt:
            print("  [saving] checkpoint")
            agent.save_models(
                settings["checkpoint_settings"]["save_name_chkpt"][0]
            )

            # TODO creare una funzione
            data_to_save = {
                 "idxStartGame": game
            }
            tmp_path = os.path.join(
                settings["paths"]["path_networks"],
                settings["checkpoint_settings"]["save_name_chkpt"][1] + ".json"
            )
            with open(tmp_path, "w") as fp:
                json.dump(data_to_save, fp)


def str2bool(arg):
    _str = arg.lower()
    return True if _str == "true" else False


def main(ip, cuda_sim, cuda_net, algo_type, algo_name, acc, use_join, dvs_on, dt, path_sub_main):
    # creation json file
    settings_file_creation(
        ip,
        cuda_sim,
        cuda_net,
        algo_type,
        algo_name,
        acc,
        use_join,
        dvs_on,
        dt
    )

    # open json file - creation folders - modify settings - save json
    with open("./settings.json", "r") as fp:
        settings = json.load(fp)
    settings = creation_dir_tree(settings, path_sub_main)

    tmp_path = os.path.join(settings["paths"]["path"], "general_settings.json")
    with open(tmp_path, "w") as fp:
        json.dump(settings, fp)
    
    # creation settings env file
    tmp_path = os.path.join(settings["paths"]["path"], "env_settings.json")
    with open(tmp_path, "w") as fp:
        json.dump(settings["airsim_settings"]["airsim_settings_file"], fp)
    
    experiment(os.path.join(settings["paths"]["path"], "general_settings.json"))


if __name__ == "__main__":
    args = parser.parse_args()
    
    main(
        args.ip,
        args.cuda_sim,
        args.cuda_net,
        args.algo_type,
        args.algo_name,
        acc=args.acc,
        use_join=str2bool(args.use_join),
        dvs_on=str2bool(args.dvs_on),
        dt=0.03,
        path_sub_main=args.path
    )