import os
import subprocess
import time
import signal

class LaunchEnv():
    def __init__(
        self,
        path_env,
        path_exp,
        name_file_settings,
        launch_settings,
        reset_env=None):
        
        self.path_env = path_env
        self.path_exp = path_exp
        self.name_file_settings = name_file_settings
        self.launch_settings = launch_settings
        self.reset_env = reset_env

        self.env_subprocess = None
        self.reset_env_cntr = None

        if self.reset_env is not None:
            self.reset_env_cntr = 0
        

    def _update_and_run(self):
        if self.reset_env is not None:
            if self.reset_env_cntr == self.reset_env-1:
                self._delete()
                self._run()
                self.reset_env_cntr = 0
                flag = True
            else:
                self.reset_env_cntr += 1
                flag = False
        return flag

    def __call__(self, flag):
        _flag = self._run() if flag else self._update_and_run()
        return flag if flag else _flag
            
    def _run(self):
        self.env_subprocess = subprocess.Popen(
            [self.path_env,
            *self.launch_settings,
            os.path.join(self.path_exp, self.name_file_settings)],
            preexec_fn=os.setsid
        )
        time.sleep(20)  # wait the simulation start

    def _delete(self):
        os.killpg(os.getpgid(self.env_subprocess.pid), signal.SIGTERM)
        time.sleep(10)
        self.env_subprocess = None
