import json
import numpy as np
import os

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as fnc


class AgentD2QN():  # in questo caso uso il ddqn
    def __init__(
        self,
        n_actions,
        replace,
        batch_size,
        q_next,
        q_eval,
        memory,
        gamma=.99,
        eps=1.,
        eps_end=.1,
        eps_dec=1e-5,
        chkpt_dir="./tmp"):

        self.n_actions = n_actions
        self.gamma = gamma
        self.eps = eps
        self.eps_end = eps_end
        self.eps_dec = eps_dec
        self.replace = replace
        self.chkpt_dir = chkpt_dir
        self.batch_size = batch_size
        self.learn_step_cntr = 0

        self.q_next = q_next
        self.q_eval = q_eval
        self.memory = memory

    def choose_action(self, obs):
        if np.random.random() < self.eps:
            action = np.random.choice(list(range(self.n_actions)))
        else:
            obs = torch.tensor(
                obs[None, ...],
                dtype=torch.float,
                device=self.q_eval.device
            )  # added a new dimension (the dimension used for the batch)
            actions = self.q_eval(obs)
            action = torch.argmax(actions).item()
        return action
    
    def replace_net(self):
        if self.replace is not None and \
                self.learn_step_cntr % self.replace == 0:
            self.q_next.load_state_dict(self.q_eval.state_dict())
            
    def decrement_eps(self):
        self.eps = self.eps_end \
            if self.eps <= self.eps_end else self.eps - self.eps_dec

    def store_memories(self, state, action, reward, state_, done):
        self.memory.update_memories(state, action, reward, state_, done)

    def sample_memories(self):
        states, actions, rewards, states_, dones = \
            self.memory.get_random_memories()
        states = torch.tensor(states, device=self.q_eval.device)
        actions = torch.tensor(actions, device=self.q_eval.device)
        rewards = torch.tensor(rewards, device=self.q_eval.device)
        states_ = torch.tensor(states_, device=self.q_eval.device)
        dones = torch.tensor(dones, device=self.q_eval.device)
        return states, actions, rewards, states_, dones

    def save_models(self, name=None):
        if name is None:
            # sto salvando solo il modello
            name_next = None
            name_eval = None
        else:
            # sto salvando un chkpt, quindi ho bisogno anche di alcuni parametri
            name_next = name + "_next.pt"
            name_eval = name +"_eval.pt"

            data_to_save = {
                 "eps": self.eps,
                 "eps_end": self.eps_end,
                 "eps_dec": self.eps_dec
            }
            with open(os.path.join(self.chkpt_dir, name + ".json"), "w") as fp:
                json.dump(data_to_save, fp)

            self.memory.save_chkpt(name=name)  # salvo la memoria

        self.q_next.save_chkpt(name_next)
        self.q_eval.save_chkpt(name_eval)

    def load_models(self, load_optimizer=False, name=None):
        if name is None:
            name_next = None
            name_eval = None
        else:
            name_next = name + "_next.pt"
            name_eval = name + "_eval.pt"
        self.q_next.load_chkpt(load_optimizer, name_next)
        self.q_eval.load_chkpt(load_optimizer, name_eval)

        if load_optimizer:  # sto facendo il restore del chkpt, quindi non ho ancora finito il training
            with open(os.path.join(self.chkpt_dir, name + ".json"), "r") as f:
                data_to_load = json.load(f)
                self.eps, self.eps_end, self.eps_dec = data_to_load.values()
            
            self.memory.load_chkpt(name=name)

    def learn(self):
        if self.memory.index_memory < self.batch_size:  # faccio questo per aspettare che si riempia la batch. Un'altra cosa ancor migliore sarebbe prendere e farlo giocare a caso
            return

        self.q_eval.optimizer.zero_grad()
        self.replace_net()  # lo faccio subito, in modo che anche la prima net sia allineata
        states, actions, rewards, states_, dones = self.sample_memories()
        # q_pred = self.q_eval(states)[actions] non posso fare così in quanto actions ha dime: batch_size x n_actions
        indices = np.arange(self.batch_size)
        q_pred = self.q_eval(states)[indices, actions]
        q_next = self.q_next(states_)
        q_eval = self.q_eval(states_)
        max_actions = torch.argmax(q_eval, dim=1)
        
        # se è finito, le rewards sono r, altrimenti sono r+gamma+Q'
        q_next[dones] = 0.
        q_target = rewards + self.gamma*q_next[indices, max_actions]

        loss = self.q_eval.loss(q_pred, q_target).to(self.q_eval.device)
        loss.backward()
        self.q_eval.optimizer.step()

        self.decrement_eps()
        self.learn_step_cntr += 1
