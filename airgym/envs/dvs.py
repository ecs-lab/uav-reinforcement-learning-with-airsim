import numpy as np
from v2ecore.emulator import EventEmulator
import time
import cv2

import matplotlib.pyplot as plt


class EventGenerator:
    def __init__(self, img_shape, plot=False, device="cuda"):
        self.emulator = EventEmulator(
            pos_thres=.2,
            neg_thres=.2,
            sigma_thres=.03,
            cutoff_hz=300,
            leak_rate_hz=.01,
            shot_noise_rate_hz=.001,
            device=device
        )
        
        self.plot = plot
        # occhio, che airism dà l'immagine flippata
        self.img_shape = img_shape[::-1]  
        self.rgb_image_shape = [*self.img_shape, 3]

        self.cntr_shape0 = 0

        self.cntr_tmp = 0

        self.fig, self.ax = None, None
        if self.plot:  # non ho verificato se questo if funzioni
            self.fig, self.ax = plt.subplots(1, 1)

    def _image_conversion(self, img):
        img = np.fromstring(img[0].image_data_uint8, dtype=np.uint8)
        if len(img) == np.prod(self.rgb_image_shape):
            img = np.reshape(img, self.rgb_image_shape)
        else:
            img = np.zeros(self.rgb_image_shape, dtype=np.uint8)
            print("img shape 0")
            self.cntr_shape0 += 1

        return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    def _events_conversion(self, events):
        events_img = np.zeros(self.img_shape, np.int8)
        events_img[events[:, 2].astype(np.int8), \
            events[:, 1].astype(np.int8)] = events[:, -1]
        return events_img

    def conversion(self, img, t):
        events = self.emulator.generate_events(img, t)

        img_events = self._events_conversion(events) \
            if events is not None else None

        self.cntr_tmp += 1

        if self.plot:
            self._plot_camera(img_events)
        
        return img_events
    
    def _plot_camera(self, img, event_camera=True):
        if self.fig is None:
            self.fig, self.ax = plt.subplots(1, 1)
        if img is not None:
            if event_camera:
                img = self._conversion_rgb(img)
            self.ax.cla()
            self.ax.imshow(img, cmap="viridis")
            plt.draw()
            plt.pause(.01)

    def _conversion_rgb(self, img): # da dvs a rgb
        out = np.zeros((*self.img_shape, 3), dtype=np.uint8)
        out[:, :, 0] = np.clip(img, 0, 1) * 255
        out[:, :, 2] = np.clip(img, -1, 0) * -255
        return out

    def plot_camera(self, img):
        self._plot_camera(img)

    def reset(self):
        self.emulator.reset()
