import json
import airsim
import glob
import numpy as np
import shutil
import os
import pandas as pd
import matplotlib.image as mpimg
import cv2
import time

DEB = False
_br=10_000_000

def movment(cl: airsim.MultirotorClient, cmd, t, acc, mov_com, path_record, pic, img_request, rgb_image_shape=None):
    global _br
    _br=10_000_000
    cmd=[cmd[1],cmd[0],cmd[2]]
    
    if mov_com:
        data, img_time, coll, pos = movmentj(cl, cmd, t, path_record, pic)
        data = conversion_from_png(data)
    else:
        data, img_time, coll, pos = movmenta(cl, cmd, t, acc, pic, img_request)
        data = conversion_from_airsim(data, rgb_image_shape)
    if DEB: print(img_time, coll, pos)
    return data, img_time, coll, pos


def movment_old(cl: airsim.MultirotorClient, cmd, t, acc, mov_com, path_record, pic, img_request, rgb_image_shape=None):
    # cmd=[cmd[1],cmd[0],cmd[2]]
    cl.simPause(True)
    def move_managed_old(c: airsim.MultirotorClient, v, duration, clock_speed, pic, img_request):
        global _br
        c.moveByVelocityAsync(*v, duration=duration)    
        eps=0.05
        t0 = c.getMultirotorState().timestamp
        elapsed = 0
        _img=list()
        _time_list=list()
        while True:
            dt = duration - elapsed
            dt /= clock_speed
            c.simContinueForTime(dt)   
            tt = c.getMultirotorState().timestamp
            elapsed = (tt-t0)*1e-9
            if elapsed > duration-eps:
                break
        img_dt=duration/pic
        img_esp=img_dt/10
        t0 = c.getMultirotorState().timestamp
        elapsed = 0
        for i in range(pic):  
            while True:
                dt = img_dt*(i+1) - elapsed
                dt /= acc
                c.simContinueForTime(dt)
                t = c.getMultirotorState().timestamp
                elapsed = (t-t0)*1e-9
                if elapsed > img_dt*(i+1)-img_esp:
                    break

            # new
            _im=c.simGetImages([img_request])
            _im = np.fromstring(_im[0].image_data_uint8, dtype=np.uint8)
            _im = np.reshape(_im, rgb_image_shape) \
            if len(_im) == np.prod(rgb_image_shape) \
                else np.zeros(rgb_image_shape, dtype=np.uint8)
            counter=0
            _flag=False
            while counter<=10 and np.sum(_im)<=1_000_000 and \
            (np.sum(_im)>_br+_br*0.1 or np.sum(_im)<_br-_br*0.1): # len(_im2) != np.prod(rgb_image_shape): # _im is None:
                print(':', end='')
                if counter == 10:
                    _flag=True
                    np.save(f"./data_alberto/pics/bug.npy", _im)
                    # c.simContinueForTime(0.01/acc)
                    # t = c.getMultirotorState().timestamp
                _im=c.simGetImages([img_request])
                _im = np.fromstring(_im[0].image_data_uint8, dtype=np.uint8)
                _im = np.reshape(_im, rgb_image_shape) \
                if len(_im) == np.prod(rgb_image_shape) \
                    else np.zeros(rgb_image_shape, dtype=np.uint8)
                counter+=1
            if not _flag: _br=np.sum(_im)
            
            _img=[*_img, _im]
            _time_list=[*_time_list, t*1e-9] 

            # old
            # _img=[*_img, c.simGetImages([img_request])]
            # _time_list=[*_time_list, t*1e-9] 

        return _img, _time_list

    data, img_time = move_managed_old(cl, cmd, t, acc, pic, img_request)
    coll = cl.simGetCollisionInfo().has_collided
    pos = cl.getMultirotorState().kinematics_estimated.position
    data = conversion_from_airsim_old(data, rgb_image_shape)
    if DEB: print(img_time, coll, pos)
    return data, img_time, coll, pos

# ----------------------- JOIN -----------------------

def movmentj(cl: airsim.MultirotorClient, cmd, t, path_record, pic):
    cl.simPause(True)
    cl.startRecording()
    tmstp1=cl.getMultirotorState().timestamp
    mov_default(cl, cmd, t)
    tmstp2=cl.getMultirotorState().timestamp
    cl.stopRecording()
    data, time_list = req_dataj(tmstp1,tmstp2,path_record,pic,t)
    coll=cl.simGetCollisionInfo().has_collided
    pos = cl.getMultirotorState().kinematics_estimated.position

    return data, time_list, coll, pos

def mov_default(cl: airsim.MultirotorClient, cmd, tim):
    obj = cl.moveByVelocityBodyFrameAsync(cmd[0],cmd[1],cmd[2],tim)
    cl.simPause(False)
    obj.join()
    cl.simPause(True)

def req_dataj(tsimstart, timsimend, path_record, pic, t):
    file_path=path_record+"/*/airsim_rec.txt"
    for f in glob.glob(file_path):
        tline,img=get_dataj(f)
    if timsimend*1e-9-tsimstart*1e-9<t:
        sam=(timsimend*1e-9-tsimstart*1e-9)/pic
    else: sam=t/pic
    if DEB: print("sam: ", sam)
    i=1
    p=list()
    time_list = list()
    for idx, e in enumerate(tline):
        if (e*1e-3-tsimstart*1e-9)>=(i*(sam-sam/10)) and i<=pic:
            p=[*p,read_img(path_record, img[idx])]
            time_list = [*time_list, e*1e-3]
            i+=1
        elif i>pic: break
    if len(p)==2: 
        p=[*p,read_img(path_record, img[-1])]
        time_list = [*time_list, tline[-1]*1e-3]
        if DEB: print((tline[-1]-tline[0])*1e-3)
    shutil.rmtree(path_record)
    os.mkdir(path_record)
    return p, time_list

def get_dataj(filepath):
    data = pd.read_csv(filepath, delimiter="\t")
    t = data.iloc[:, 1].to_list()
    t = [float(_) for _ in t]
    img = data.iloc[:, -1].to_list()
    if DEB: print("len t: ", len(t))
    return t, img 

def read_img(img_path, img_name):
    filepath=img_path + "/" + list(os.walk(img_path))[0][1][0] + "/images/" + img_name
    img = cv2.imread(filepath)
    return img

# ----------------------- ACTIVE -----------------------

def movmenta(cl: airsim.MultirotorClient, cmd, t, acc, pic, img_request):
    cl.simPause(True)
    data, time_list = mov_active(cl, cmd, t, acc, pic, img_request)
    coll = cl.simGetCollisionInfo().has_collided
    pos = cl.getMultirotorState().kinematics_estimated.position
    return data, time_list, coll, pos

def mov_active(cl: airsim.MultirotorClient, cmd, tim, acc, pic, img_request):
    img_dt=tim/pic

    def move_managed(c: airsim.MultirotorClient, v, duration, acc):
        img=list()
        time_list=list()
        c.moveByVelocityBodyFrameAsync(*v, duration=duration)   
        img_esp=img_dt/10
        t0 = c.getMultirotorState().timestamp
        elapsed = 0
        for i in range(pic):  
            while True:
                dt = img_dt*(i+1) - elapsed
                dt /= acc
                c.simContinueForTime(dt)
                t = c.getMultirotorState().timestamp
                elapsed = (t-t0)*1e-9
                if elapsed > img_dt*(i+1)-img_esp:
                    break
            img=[*img, c.simGetImages([img_request])]
            time_list=[*time_list, t*1e-9] 
            if DEB: print((t-t0)*1e-9)
        return img, time_list

    return move_managed(cl, cmd, tim, acc)

# ----------------------- UTILITIES -----------------------

_am_step = 1

def conversion_from_airsim(imgs, rgb_image_shape):
    # global _am_step
    # folder = "garb"
    img_out = list()
    # cntr = 1
    for _img in imgs:
        img = np.fromstring(_img[0].image_data_uint8, dtype=np.uint8)
        img = np.reshape(img, rgb_image_shape) \
            if len(img) == np.prod(rgb_image_shape) \
                else np.zeros(rgb_image_shape, dtype=np.uint8)
        # np.save(f"./data_alberto/pics/{folder}/rgb_{cntr}_{_am_step}.npy", img)
        img_out.append(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))
    #     np.save(f"./data_alberto/pics/{folder}/gray_{cntr}_{_am_step}.npy", img_out[-1])
    #     cntr += 1
    # _am_step += 1
    return img_out

def conversion_from_airsim_old(imgs, rgb_image_shape):
    global _am_step
    folder = "garb3"
    img_out = list()
    cntr = 1
    for _img in imgs:
        np.save(f"./data_alberto/pics/{folder}/rgb_{cntr}_{_am_step}.npy", _img)
        img_out.append(cv2.cvtColor(_img, cv2.COLOR_BGR2GRAY))
        np.save(f"./data_alberto/pics/{folder}/gray_{cntr}_{_am_step}.npy", img_out[-1])
        cntr += 1
    _am_step += 1
    return img_out


def conversion_from_png(imgs):
    global _am_step
    folder = "garb3"
    cntr = 1
    for _img in imgs:
        np.save(f"./data_alberto/pics/{folder}/rgb_{cntr}_{_am_step}.npy", _img)
        cntr += 1
    _am_step += 1
    return [cv2.cvtColor(_img, cv2.COLOR_BGR2GRAY) for _img in imgs]
