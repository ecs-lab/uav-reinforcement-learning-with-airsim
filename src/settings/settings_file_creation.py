import json


def settings_file_creation(
    ip,
    cuda_sim,
    cuda_net,
    algo_type,
    algo_name,
    acc,
    use_join,
    dvs_on,
    dt):


    algo_name = algo_name + algo_type.upper()

    dict_to_save = {
        "airsim_settings": {
            "airsim_settings_file": {
                "SettingsVersion": 1.2,
                "SimMode": "Multirotor",
                "LocalHostIp": ip,
                "ClockSpeed": acc

                # "CameraDefaults": {
                #     "CaptureSettings": [
                #     {
                #         "ImageType": 0,
                #         "Width": 256, # 256,
                #         "Height": 144, # 144,
                #         "FOV_Degrees": 90, # 90,
                #         "AutoExposureSpeed": 0, # 100,
                #         "AutoExposureBias": 1, # 0,
                #         "AutoExposureMaxBrightness": 2., # 0.64,
                #         "AutoExposureMinBrightness": 2., # 0.03,
                #         "MotionBlurAmount": 0, # 0,
                #         "TargetGamma": 3.0, # 1.0,
                #         "ProjectionMode": "", # "",
                #         "OrthoWidth": 5.12 # 5.12
                #     }]

                    # "NoiseSettings": [
                    # {             
                    #     "Enabled": False,
                    #     "ImageType": 0,

                    #     "RandContrib": 0.2,
                    #     "RandSpeed": 100000.0,
                    #     "RandSize": 500.0,
                    #     "RandDensity": 2,

                    #     "HorzWaveContrib":0.03,
                    #     "HorzWaveStrength": 0.08,
                    #     "HorzWaveVertSize": 1.0,
                    #     "HorzWaveScreenSize": 1.0,

                    #     "HorzNoiseLinesContrib": 1.0,
                    #     "HorzNoiseLinesDensityY": 0.01,
                    #     "HorzNoiseLinesDensityXY": 0.5,

                    #     "HorzDistortionContrib": 1.0,
                    #     "HorzDistortionStrength": 0.002
                    # }]
                # }

            },
            "airsim_env_args": ["-windowed",
                                "-RenderOffscreen",
                                "--graphicsadapter=" + cuda_sim,
                                "--settings"],
            "airsim_cuda": cuda_sim,
            "airsim_env_path": "/home/lzanatta/tools/Binaries/startBenchmark/LinuxNoEditor/startBenchmark.sh", #newStartBenchmarkV3
        },
        "droneairenv_settings": {
            "img_shape": [256, 144],
            "step_v": 1.,
            "step_t": 1.,
            "gamma_p": 1,
            "gamma_a": -.1,
            "gamma_c": -10,
            "n_actions_stop": 250,  # if the drone made more then 250 the game end (it will be count as a collision)
            "goal_y": 150,
            "use_join": use_join,
            "frames": 3,
            "plot": False,
            "ip": ip,
            "dvs_on": dvs_on
        },
        "simulation_settings":{
            "algo_type": algo_type,
            "algo_name": algo_name,
            "eval_mode": True,  # True fa il test
            "env_name": "DroneEnv",
            "idxStartGame": 0,
            "nGames_train": 5_001, # prima era 10_000 # games using in the train - if eval_mode is False
            "nGames_eval": 100,  # games using in the test - if eval_mode is True
            "replace": 500,
            "buffer_size": 10_000,
            "batch_size": 32,
            "cuda": cuda_net,
            "lr": 1e-4,
            "eps": [1., .1, 9e-4], # agente randomicodel training [0, 0, 0]
            "gamma": .99
        },
        "checkpoint_settings":{
            "load_chkpt": False,  # start training from checkpoint
            "save_chkpt": True,
            "ngames_save_chkpt": 500,  # save data every ngames (checkpoint)
            "save_name_chkpt": ["net_chkpt", "main_params"]  # where to save/load checkpoint data
        },
        "utils": {
            "ngames_save": 100,
            "get_time_stats": True,
            "reset_env": 100,  # reset the enveronment every reset_env games
            "create_dataset_flag": False
        },
        "paths": {
            "main_save_folder": "data",  # folder where all experiments are stored
            "sub_main_save_folder": "",  # folder where the current experiment is stored
            "path": "",  # path to the folder of the current experiment
            "path_networks": "",
            "path_graphs": "",
            "path_dataset": "",
            "main_script_path": "/home/lzanatta/lavoro/rl/tmp_accelerazioni/sne-and-rl-airism/main.py"
        }}

    if use_join:
        dict_join = {
            "Enabled": False,
            "RecordOnMove": True,
            "RecordInterval": dt,
            "Folder": "",
            "Cameras": [{
                "CameraName": "0",
                "ImageType": 0,
                "PixelsAsFloat": False,
                "Compress": True
            }]
        }
        dict_to_save["airsim_settings"]["airsim_settings_file"]["Recording"] = \
            dict_join
    
    settings_file = json.dumps(dict_to_save)

    with open("./settings.json", "w") as f:
        f.write(settings_file)